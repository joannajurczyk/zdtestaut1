package zestaw1;

import java.util.Scanner;

public class Zadanie8 {

    public static void main(String[] args) {

        Scanner scanner = new Scanner(System.in);

        int a = scanner.nextInt();
        int b = scanner.nextInt();

        if(a >= b) {
            return;
        }

        int whileSum = 0;
        int whileA = a;
        int whileB = b;
        while(whileA <= whileB) {
            whileSum = whileSum + whileA;
            whileA++;
        }
        System.out.println(whileSum);

        int doWhileSum = 0;
        int doWhileA = a;
        int doWhileB = b;
        do {
            doWhileSum = doWhileSum + doWhileA;
            doWhileA++;
        } while(doWhileA <= doWhileB);
        System.out.println(doWhileSum);

        int forSum = 0;
        for(int forA = a; forA <= b; forA++) {
            forSum = forSum + forA;
        }
        System.out.println(forSum);
    }

}
