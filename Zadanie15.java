package zestaw1;

import java.util.Scanner;

public class Zadanie15 {

    public static void main(String[] args) {

        Scanner scanner = new Scanner(System.in);

        int n = scanner.nextInt();

        // 23456
        // 2 + 3 + 4 + 5 + 6 = 20
        // (2 + 4 + 6)/3 -> parzyste -> 4
        // (3 + 5)/2 -> nieparzyste -> 4
        // parzyste/nieparzyste -> 4/4 = 1

        // % 10 -> cyfra

        int sum = 0;
        int evenSum = 0;
        int evenCounter = 0;
        int oddSum = 0;
        int oddCounter = 0;
        while(n > 0) {
            int digit = n % 10;
            sum = sum + digit;

            if(digit % 2 == 0) {
                evenSum = evenSum + digit;
                evenCounter++;
            } else {
                oddSum = oddSum + digit;
                oddCounter++;
            }

            n = n / 10; /// 23456/10 -> 2345.6 -> int: 2345
        }

        double evenArithemtic;
        if(evenCounter > 0) {
            evenArithemtic = (double) evenSum / (double) evenCounter;
        } else {
            evenArithemtic = 0.0;
        }
        double oddArithemic;
        if(oddCounter > 0) {
            oddArithemic = (double) oddSum / (double) oddCounter;
        } else {
            oddArithemic = 0.0;
        }

        System.out.println("Suma: " + sum);
        if(oddArithemic == 0.0) {
            System.out.println("Stosunek liczb wynosi nieskończoność");
        } else {
            System.out.println("Stosunek liczb: " + evenArithemtic / oddArithemic);
        }

    }

}
