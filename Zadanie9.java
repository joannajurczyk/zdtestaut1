package zestaw1;

import java.util.Scanner;

public class Zadanie9 {

    public static void main(String[] args) {

        Scanner scanner = new Scanner(System.in);

        int n = scanner.nextInt();

        int power = 1;  // 2^0
        while(power <= n) {
            System.out.println(power);
            power = power * 2;  // power *2 *2 *2 *2... -> 2^5 = 2*2*2*2*2
        }

        for(int pow = 1; pow <= n; pow = pow * 2) {
            System.out.println(pow);
        }

    }

}
